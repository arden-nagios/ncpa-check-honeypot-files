#!/usr/bin/python

import argparse
import hashlib
import os.path
import re
import sys

# File reading blocksize (4 MiB)
BLOCKSIZE=4194304

# Nagios states
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

def check_file(entry):
    """
    Verifies that the specified file exists, and matches the
    provided checksum.
    :param entry: string containing the data within the file
    :type entry: str
    """
    # Attempt to appropriately extract the relevant attributes
    try:
        file_name, hash_value, hash_type = re.split(r'\t+', entry)
    except ValueError:
        print("error: the following line is malformed:\n {}".format(entry))
        sys.exit( STATE_UNKNOWN )

    # Ensure the provided hashing type is valid
    try:
        hasher = hashlib.new(hash_type)
    except ValueError as error:
        print("error: {errstr}\n".format( errstr=str(error) ))
        sys.exit( STATE_UNKNOWN )

    # Generate a hash value for the provided file
    try:
        with open(file_name, 'rb', BLOCKSIZE) as honey_file:
            read_buf = honey_file.read(BLOCKSIZE)
            while len(read_buf) > 0:
                hasher.update(read_buf)
                read_buf = honey_file.read(BLOCKSIZE)

            hash_check = hasher.hexdigest()
    except IOError as error:
        print("CRITICAL: failed to access \'{fname}\': {errstr}".format(
                fname=file_name, errstr=error.strerror))
        sys.exit( STATE_CRITICAL )

    # If the hash is different, abort & raise the critical alarm
    if hash_check != hash_value:
        print("CRITICAL: {honey_file} checksum does not match!\n".format(
                honey_file=file_name))
        sys.exit( STATE_CRITICAL )

    return True


def main():
    # Process any necessary arguments
    usage_text = 'Verify assorted files by checksum. A tsv file of the' \
            'form:\n <filepath> <check_type> <checksum> must be provided.'
    parser = argparse.ArgumentParser(
            description=usage_text )
    parser.add_argument('-f', default='honeycomb.tsv', dest='comb_path',
            help='process the specified file list (default: ./honeycomb.tsv)')
    args = parser.parse_args()

    # Remove spurious characters from the path
    comb_path = re.sub(r'[\'"]', '', args.comb_path)

    # Ensure the honeycomb file exists
    try:
        honeycomb_file = open(comb_path, 'rb')
    except IOError as error:
        print("error: failed to access hash list file \'{fname}\': " \
                "{errstr}".format(fname=args.comb_path, errstr=error.strerror))
        sys.exit( STATE_UNKNOWN )

    check_count = 0
    for file_entry in honeycomb_file:
        if check_file(str(file_entry).rstrip()):
            check_count += 1

    print("OK: {count} honeypot files validated successfully!\n".format(
            count=check_count))

if __name__ == "__main__":
    main()
